# Albany Birth Costs

The goal of this exercise is to explore your analytical process and
presentation skills. Please address the questions below to the best of
your ability and develop materials to explain/present your analysis to
a diverse audience which will include technical and non-technical
Acuitas staff.

Thank you and good luck!

The path to this project on GitLab: https://gitlab.com/andy.choens/albany-birth-costs

## Instructions

Import the
[Births](https://gitlab.com/andy.choens/albany-birth-costs/raw/master/Births.csv)
csv file into your preferred analytical tool (R, Python, SAS, SPSS,
etc.) and answer the following questions:

0. Describe the distribution of birth costs.
   - Are birth costs normally distributed?
   - If they are skewed, what does this tell us about birth costs?
   - High Cost births are those which cost more than $10,000 dollars. What
     percent of births are high cost?
   - https://www.businessinsider.com/how-much-does-it-cost-to-have-a-baby-2018-4
1. Identify two to three risk factors for high-cost births and
   estimate the impact of these risk factors.
2. Some births have a null (missing) cost. Develop a model to estimate
   the cost of these births. Assess the accuracy of your model. 
   - What are its strengths? 
   - What are its weaknesses? 
   - What other data do you think would be helpful?
   
At the end of the hour, you will be asked to present your findings to
a mixed audience which will include members of the data science team
and non-technical Acuitas staff. Be prepared to explain and
present your findings to this diverse audience.

You may choose any mode of presentation you would like. Please submit
your code/materials to andy.choens@acuitashealth.com. We will cast
your presentation onto the Kraken.

## About the Data

- Link to the raw data (use this link or a git clone to download the data):
  [Births.csv](https://gitlab.com/andy.choens/albany-birth-costs/raw/master/Births.csv)
- This data set was obtained from the Book, "Biostatistics for the
  Biological and Health Sciences" by Triola, Triola, and Roy.
- This is the "Births" data set which is comprised of 400 births from
  upstate New York, including Albany.
- Columns:
    - ID: Unique birth identifier.
    - Facility: Name of the hospital where the birth occurred.
    - Insurance: Name of the mother's insurance, which paid for the
      birth.
    - Gender: Gender of the newborn.
        - 0 = Female
        - 1 = Male
    - LengOfStay: Duration of the hospital stay for the newborn in days.
    - Admitted: Day of the week the mother was admitted to hospital.
    - Discharged: Day of the week the infant was discharged from
      hospital.
    - Birth Weight: Birth weight of the newborn in grams.
    - Total Charges: Total cost of the birth in US dollars.
  
