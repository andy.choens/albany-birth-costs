Good morning, candidate!

As we explained during your interview, the next step in our process is a data-oriented challenge. Please review the materials at: https://gitlab.com/andy.choens/albany-birth-costs

On March 25, you will present your answers to the following questions

0. Describe the distribution of birth costs.
    - Are birth costs normally distributed?
    - If they are skewed, what does this tell us about birth costs?
    - High Cost births are those which cost more than $10,000 dollars. What percent of births are high cost?
    - https://www.businessinsider.com/how-much-does-it-cost-to-have-a-baby-2018-4
1. Identify two to three risk factors for high-cost births and estimate the impact of these risk factors.
2. Some births have a null (missing) cost. Develop a model to estimate the cost of these births. Assess the accuracy of your model.
    - What are its strengths?
    - What are its weaknesses?
    - What other data do you think would be helpful?
    
The data, instructions, etc. are all available on gitlab. You will present your findings to a panel of current Acuitas employees. This panel will include data scientists as well as staff who do not have technical backgrounds. Your goal is to present your findings to this mixed audience in a way that is compelling and understandable to all.

If you have any questions, please do not hesitate to ask. 

--andy
